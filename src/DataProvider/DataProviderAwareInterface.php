<?php
/**
 * Created by PhpStorm.
 * User: ogxone
 * Date: 13.09.15
 * Time: 13:57
 */

namespace Pentity2\Grid\DataProvider;


interface DataProviderAwareInterface 
{
    /**
     * @param DataProviderInterface $dataProvider
     * @return mixed
     */
    public function setDataProvider(DataProviderInterface $dataProvider = null);

    /**
     * @return DataProviderInterface
     */
    public function getDataProvider();
}