<?php
/**
 * Created by PhpStorm.
 * User: ogxone
 * Date: 18.09.15
 * Time: 11:07
 */

namespace Pentity2\Grid\Widget\Grid\Action;


use Pentity2\Grid\Exception\WidgetException;
use Pentity2\Grid\Widget\Grid\Action\Button\AbstractButton;
use Pentity2\Utils\Param\Param;

class ButtonFactory
{
    private static $_buttons = [
        'plain' => 'Pentity2\Grid\Widget\Grid\Action\Button\PlainButton'
    ];


    /**
     * @param array $config
     * @return AbstractButton
     * @throws WidgetException
     */
    public static function factory(Array $config)
    {
        $type = Param::get('type', $config);
        $options = Param::getArr('options', $config);

        if (!isset(static::$_buttons[$type])) {
            throw new WidgetException(sprintf('Undefined button type %s', $type));
        }
        return new static::$_buttons[$type]($options);
    }
}