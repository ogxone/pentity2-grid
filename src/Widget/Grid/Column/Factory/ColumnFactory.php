<?php
/**
 * Created by PhpStorm.
 * User: ogxone
 * Date: 10.09.15
 * Time: 15:57
 */

namespace Pentity2\Grid\Widget\Grid\Column\Factory;


use Pentity2\Grid\Exception\WidgetException;
use Pentity2\Grid\Widget\Grid\Column\ColumnInterface;
use Pentity2\Utils\Param\Param;


class ColumnFactory
{
    //@todo register namespace for factories
    private static $_columns = [
        'data' => 'Pentity2\Grid\Widget\Grid\Column\DataColumn',
        'serial' => 'Pentity2\Grid\Widget\Grid\Column\SerialColumn',
        'button' => 'Pentity2\Grid\Widget\Grid\Column\ButtonColumn'
    ];

    /**
     * @param array $config
     * @return ColumnInterface
     * @throws WidgetException
     */
    public static function factory(Array $config)
    {
        $name = Param::get('name', $config);
        $type = Param::get('type', $config);

        $options = Param::getArr('options', $config);

        if (!isset(static::$_columns[$type])) {
            throw new WidgetException(sprintf('Undefined column type %s', $type));
        }
        return new static::$_columns[$type]($name, $options);
    }
}