<?php
/**
 * Created by PhpStorm.
 * User: ogxone
 * Date: 15.09.15
 * Time: 11:05
 */

namespace Pentity2\Grid\Widget\Browse;


use Pentity2\Grid\DataProvider\DataProviderAwareInterface;
use Pentity2\Grid\DataProvider\DataProviderAwareTrait;
use Pentity2\Grid\DataProvider\DataProviderInterface;
use Pentity2\Grid\Exception\WidgetException;
use Pentity2\Grid\Widget\AbstractWidget;
use Pentity2\Grid\Widget\Pager\Pager;
use Pentity2\Grid\Widget\Sorter\Sorter;
use Pentity2\Utils\Helpers\StaticHtmlHelper as Html;
use Pentity2\Utils\Php\Includer;

class Browse extends AbstractWidget implements DataProviderAwareInterface
{
    use DataProviderAwareTrait;

    /**
     * @var $_pagerConfig Array
     */
    private $_pagerConfig;

    /**
     * @var $_browserEnclosureAttributes Array
     */
    private $_browserEnclosureAttributes;

    /**
     * @var $_browserTemplateParams Array
     */
    private $_browserTemplateParams;

    /**
     * @var $_sorterSpecs
     */
    private $_sorterSpecs;

    /**
     * @var $_sorterEnclosureAttributes Array
     */
    private $_sorterEnclosureAttributes;

    public function setPagerConfig(Array $pagerConfig)
    {
        $this->_pagerConfig = $pagerConfig;
    }

    public function getPagerConfig()
    {
        return $this->_pagerConfig;
    }

    public function setBrowseEnclosureAttributes(Array $attributes)
    {
        $this->_browserEnclosureAttributes = $attributes;
    }

    public function getBrowseEnclosureAttributes()
    {
        return $this->_browserEnclosureAttributes;
    }

    public function setSorterEnclosureAttributes(Array $attributes)
    {
        $this->_sorterEnclosureAttributes = $attributes;
    }

    public function getSorterEnclosureAttributes()
    {
        return $this->_sorterEnclosureAttributes;
    }

    public function setBrowseTemplateParams(Array $attributes)
    {
        $this->_browserTemplateParams = $attributes;
    }

    public function getBrowseTemplateParams()
    {
        return $this->_browserTemplateParams;
    }

    public function setSorterSpecs(Array $specs)
    {
        $this->_sorterSpecs = $specs;
    }

    public function getSorterSpecs()
    {
        return $this->_sorterSpecs;
    }

    public function render()
    {
        $template = $this->getOption('template');
        //pager first because it can change data provider config

        return str_replace([
            '{pager}',
            '{sorter}',
            '{browse}'
        ], [
            $this->_renderPager(),
            $this->_renderSorter(),
            $this->_renderBrowse()
        ], $template);
    }

    protected function _renderBrowse()
    {
        $template = $this->getOption('browse_template');
        $content = '';
        $data = $this->getDataProvider()->getData();
        if (null === $data) {
            $content = $this->getOption('no_data_message');
        } else {
            foreach ($data as $index => $entry) {
                $content .= Includer::template(
                    $template,
                    ['entity' => $entry, 'index' => $index] + $this->getBrowseTemplateParams()
                );
            }
        }
        return Html::tag(
            $this->getOption('browse_enclosure_tag'),
            $content,
            $this->getBrowseEnclosureAttributes()
        );
    }

    protected function _renderSorter()
    {
        if (!$this->getOption('sorter_enabled')) {
            return '';
        }
        $content = '';
        foreach ($this->getSorterSpecs() as $spec) {
            $content .= Sorter::init($spec)->render();
        }
        return Html::tag(
            $this->getOption('sorter_enclosure_tag'),
            $content,
            $this->getSorterEnclosureAttributes()
        );
    }

    protected function _renderPager()
    {
        if (!$this->getOption('pager_enabled')) {
            return '';
        }
        $this->getDataProvider()->setPaginationEnabled(true);
        return Pager::init([
                'data_provider' => $this->getDataProvider()
            ] + $this->getPagerConfig())->render();
    }

    protected function _validateInput()
    {
        if (!is_readable($template = $this->getOption('browse_template'))) {
            throw new WidgetException(sprintf(
                'Browse template unreadable or not provided. %s', $template));
        }
        if (!$this->getDataProvider() instanceof DataProviderInterface) {
            throw new WidgetException(sprintf(
                'Data provider have to be set and implement Pentity2\Grid\DataProvider\DataProviderInterface'));
        }
    }

    /**
     * @return Array
     */
    protected function _getDefaultConfig()
    {
        return [
            'pager_enabled' => false,
            'pager_config' => [],

            'sorter_enabled' => false,
            'sorter_enclosure_tag' => 'div',
            'sorter_enclosure_attributes' => [
                'class' => 'browse-sort'
            ],
            'sorter_pecs' => [],

            'template' => '{sorter}{browse}{pager}',
            'data_provider' => null,

            'browse_template' => null,
            'browse_template_params' => [],

            'browse_enclosure_tag' => 'div',
            'browse_enclosure_attributes' => [
                'class' => 'browse'
            ],

            'no_data_message' => 'Oops. Empty here!',
        ];
    }
}