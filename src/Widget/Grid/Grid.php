<?php
/**
 * Created by PhpStorm.
 * User: ogxone
 * Date: 10.09.15
 * Time: 14:22
 */

namespace Pentity2\Grid\Widget\Grid;


use Pentity2\Domain\Entity\EntityInterface;
use Pentity2\Grid\DataProvider\DataProviderAwareInterface;
use Pentity2\Grid\DataProvider\DataProviderAwareTrait;
use Pentity2\Grid\DataProvider\DataProviderInterface;
use Pentity2\Grid\Exception\WidgetException;
use Pentity2\Grid\Widget\AbstractWidget;
use Pentity2\Grid\Widget\Grid\Action\ButtonFactory;
use Pentity2\Grid\Widget\Grid\Column\Factory\ColumnFactory;
use Pentity2\Grid\Widget\Grid\Column\ColumnInterface;
use Pentity2\Grid\Widget\Grid\Column\DataColumn;
use Pentity2\Grid\Widget\Grid\Column\Filterable;
use Pentity2\Grid\Widget\Pager\Pager;
use Pentity2\Utils\Helpers\StaticHtmlHelper as Html;
use Pentity2\Utils\Param\Param;

class Grid extends AbstractWidget implements DataProviderAwareInterface
{
    use DataProviderAwareTrait;

    /**
     * @var $_columns Array
    */
    private $_columns = [];

    /**
     * @var $_columns Array
     */
    private $_pagerConfig = [];

    /**
     * @var $_columns Array
     */
    private $_FilterActionButtons = [];

    /**
     * @return Array
     */
    public function getFilterActionButtons()
    {
        return $this->_FilterActionButtons;
    }

    /**
     * @param Array $FilterActionButtons
     */
    public function setFilterActionButtons(Array $FilterActionButtons)
    {
        $this->_FilterActionButtons = $FilterActionButtons;
    }

    /**
     * @param array $columns
     * @throws WidgetException
     */
    public function setColumns(Array $columns)
    {
        foreach ($columns as $spec) {
            $type = Param::get('type', $spec);
            $name = Param::get('name', $spec);
            if (!$type && !$name) {
                throw new WidgetException(sprintf(
                    'Columns specification failure. Either type or name have to be provided'
                ));
            }
            $this->_columns[] = ColumnFactory::factory($spec);
        }
    }

    public function getColumns()
    {
        return $this->_columns;
    }

    public function setPagerConfig(Array $pagerConfig)
    {
        $this->_pagerConfig = $pagerConfig;
    }

    public function getPagerConfig()
    {
        return $this->_pagerConfig;
    }

    /**
     * @throws WidgetException
     */
    protected function _validateInput()
    {
        if (!$this->getDataProvider() instanceof DataProviderInterface) {
            throw new WidgetException(sprintf(
                'data provider have to implement Pentity2\Grid\DataProvider\DataProviderInterface'));
        }
    }

    public function render()
    {
        $template = $this->getOption('template');
        //pager first because it can change data provider config

        return str_replace([
            '{pager}',
            '{actions}',
            '{grid}'
        ], [
            $this->_renderPager(),
            $this->_renderActions(),
            $this->_renderGrid()
        ], $template);
    }

    private function _renderGrid()
    {
        $tableContent = $headContent = $bodyContent = '';
        foreach ($this->getColumns() as $column) {
            /**
             * @var $column ColumnInterface
            */
            $headContent .= $column->renderHeaderCell();
        }
        if ($this->getOption('filter_enabled')) {
            $bodyContent .= $this->_renderFilter();
        }
        $data = $this->getDataProvider()->getData();
        if (null === $data) {
            $bodyContent = Html::tag('tr', Html::tag('td', $this->getOption('no_data_message'), [
                'colspan' => count($this->getColumns())
            ]), []);
        } else {
            foreach ($data as $entry) {
                /**
                 * @var $entry EntityInterface
                 */
                $rowContent = '';
                foreach ($this->getColumns() as $column) {
                    /**
                     * @var $column ColumnInterface
                     */
                    if (
                        !$column instanceof DataColumn ||
                        (
                            $column instanceof DataColumn &&
                            isset($entry->{$column->getName()})
                        )
                    ) {
                        $rowContent .= $column->renderDataCell($entry);
                    }
                }
                $bodyContent .= Html::tag('tr', $rowContent, []);
            }
        }
        $tableContent .= Html::tag('thead', $headContent, []);
        $tableContent .= Html::tag('tbody', $bodyContent, []);
        return Html::tag('table', $tableContent, []);
    }

    private function _renderFilter()
    {
        $filterRowContent = '';
        foreach ($this->getColumns() as $column) {
            /**
             * @var $column DataColumn
             */
            if ($column instanceof Filterable && $column->getOption('filter_enabled')) {
                $filterRowContent .= $column->renderFilterCell(); //@todo provide value here
            } else {
                $filterRowContent .= Html::tag('td', '', []);
            }
        }
        return Html::tag('tr', $filterRowContent, []);
    }

    private function _renderPager()
    {
        if (!$this->getOption('pager_enabled')) {
            return '';
        }
        $this->getDataProvider()->setPaginationEnabled(true);
        return Pager::init([
            'data_provider' => $this->getDataProvider()
        ] + $this->getPagerConfig())->render();
    }

    private function _renderActions()
    {
        $content = '';
        if ($this->getOption('filter_enabled')) {
            foreach ($this->getFilterActionButtons() as $button) {
                $content .= ButtonFactory::factory($button)->render();
            }
        }
        if (empty($content)) {
            return '';
        }
        return Html::tag(
            $this->getOption('actions_bar_tag'),
            $content,
            $this->getOption('actions_bar_attributes')
        );
    }

    /**
     * @return Array
     */
    protected function _getDefaultConfig()
    {
        return [
            'actions_bar_attributes' => [
                'class' => 'dBox'
            ],
            'actions_bar_tag' => 'div',

            'filter_enabled' => false,
            'filter_action_buttons' => [
                [
                    'type' => 'plain',
                    'options' =>[
                        'label' => 'Apply filter',
                        'attributes' => [
                            'onclick' => 'p2Grid.applyFilter()'
                        ]
                    ]
                ],
                [
                    'type' => 'plain',
                    'options' => [
                        'label' => 'Clear filter'
                    ]
                ]
            ],

            'pager_enabled' => false,
            'pager_config' => [

            ],
            'actions_enabled' => false,
            'actions_config' => [

            ],
            'template' => '{actions}{grid}{pager}',
            'no_data_message' => 'Oops. Empty here!',
        ];
    }
}