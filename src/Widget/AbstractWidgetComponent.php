<?php
/**
 * Created by PhpStorm.
 * User: ogxone
 * Date: 11.09.15
 * Time: 13:23
 */

namespace Pentity2\Grid\Widget;


use Pentity2\Grid\Exception\WidgetException;
use Pentity2\Utils\Options\OptionsTrait;

abstract class AbstractWidgetComponent
{
    use OptionsTrait;

    public function __construct(Array $options)
    {
        $this->configure($options);
    }

    public function configure(Array $options)
    {
        $this->setOptions($this->_getDefaultConfig());
        $this->setOptions($options, true);
        $this->_validateInput();
    }

    /**
     * @return Array
     */
    abstract protected function _getDefaultConfig();

    /**
     * @throws WidgetException
     */
    protected function _validateInput()
    {}
}
