<?php
/**
 * Created by PhpStorm.
 * User: ogxone
 * Date: 10.09.15
 * Time: 15:56
 */

namespace Pentity2\Grid\Widget\Grid\Column;


use Pentity2\Grid\Widget\AbstractWidgetComponent;
use Pentity2\Grid\Widget\AttributesAwareTrait;
use Pentity2\Utils\ArrayUtils\ArrayUtils;
use Pentity2\Utils\Helpers\StaticHtmlHelper as Html;

abstract class AbstractColumn  extends AbstractWidgetComponent implements ColumnInterface
{
    use AttributesAwareTrait;

    protected $_name;
    protected $_headAttributes = [];
    protected $_title;

    public function __construct($name, Array $options = [])
    {
        $this->_name = $name;
        parent::__construct($options);
    }

    public function setTitle($title)
    {
        $this->_title = $title;
    }

    public function getTitle()
    {
        if (null !== $this->_title) {
            return $this->_title;
        } else {
            return ucfirst(trim(str_replace('_', ' ', $this->_name)));
        }
    }

    public function getName()
    {
        return $this->_name;
    }

    public function setHeadAttributes(Array $attributes)
    {
        $this->_headAttributes = ArrayUtils::merge($this->_headAttributes, $attributes);
    }

    public function getHeadAttributes()
    {
        return $this->_headAttributes;
    }

    public function renderHeaderCell()
    {
        $content = $this->getTitle();
        if ($this->getOption('encode')) {
            $content = Html::encode($content);
        }
        return Html::tag('th', $this->_prepareHeaderCell($content), $this->getHeadAttributes());
    }

    protected function _prepareHeaderCell($content)
    {
        return $content;
    }

    protected function _getDefaultConfig()
    {
        return [
            'encode' => true,
            'attributes' => [],
            'head_attributes' => [],
        ];
    }
}