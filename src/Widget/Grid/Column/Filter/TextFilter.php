<?php
/**
 * Created by PhpStorm.
 * User: ogxone
 * Date: 11.09.15
 * Time: 13:26
 */

namespace Pentity2\Grid\Widget\Grid\Column\Filter;


use Pentity2\Utils\Helpers\StaticHtmlHelper;

class TextFilter extends AbstractFilter
{
    public function render()
    {
        return StaticHtmlHelper::tag('input', $this->getValue(), $this->getAttributes());
    }
}