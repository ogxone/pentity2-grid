<?php
/**
 * Created by PhpStorm.
 * User: ogxone
 * Date: 10.09.15
 * Time: 14:20
 */

namespace Pentity2\Grid\Widget;


interface RenderableInterface
{
    public function render();
}