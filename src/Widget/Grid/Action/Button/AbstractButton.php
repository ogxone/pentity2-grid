<?php
/**
 * Created by PhpStorm.
 * User: ogxone
 * Date: 18.09.15
 * Time: 11:06
 */

namespace Pentity2\Grid\Widget\Grid\Action\Button;


use Pentity2\Grid\Widget\AbstractWidgetComponent;
use Pentity2\Grid\Widget\AttributesAwareTrait;
use Pentity2\Grid\Widget\RenderableInterface;

abstract class AbstractButton extends AbstractWidgetComponent implements RenderableInterface
{
    use AttributesAwareTrait;

    /**
     * @return Array
     */
    protected function _getDefaultConfig()
    {
        return [
            'encode_label' => true,
            'attributes' => []
        ];
    }
}