<?php
/**
 * Created by PhpStorm.
 * User: ogxone
 * Date: 13.09.15
 * Time: 12:44
 */

namespace Pentity2\Grid\DataProvider;


use Pentity2\Domain\Entity\Collection\EntityCollectionInterface;
use Pentity2\Grid\DataProvider\Exception\DataProviderException;
use Pentity2\Infrastructure\Mapper\Manager\Mappers\SqlMapper;
use Pentity2\Infrastructure\Mapper\SqlMapperInterface;
use Zend\Db\Sql\Expression;
use Zend\Db\Sql\Select;

class SqlMapperDataProvider extends AbstractDataProvider
{
    /**
     * @var $_mapper SqlMapperInterface
    */
    protected $_mapper;

    /**
     * @var $_select Select
     */
    protected $_select;


    /**
     * @var $_data EntityCollectionInterface|null
     */
    protected $_data;
    private $_count;

    public function __construct(SqlMapper $mapper, Array $params)
    {
        $this->_mapper = $mapper;
        $this->_select = $mapper->getSelect();
        parent::__construct($params);
    }

    public function getData()
    {
        if ($this->getPaginationEnabled()) {
            $limit = $this->getPageSize();
            $offset = ($this->getPage() - 1) * $limit;
        } else {
            $limit = $offset = null;
        }

        $this->_mapper->setSelect($this->_select);
        $data = $this->_mapper->fetchAll($limit, $offset);
        if (null === $data) {
            $this->_count = 0;
        } else {
            $this->_count = $data->count();
        }
        return $data;
    }

    public function getTotalCount()
    {
        $select = clone $this->_select;
        $select->limit(1)
            ->offset(0)
            ->columns(['count' => new Expression('COUNT(*)')]);
        $this->_mapper->setSelect($select);
        $ret = $this->_mapper->fetchOne();
        if (null === $ret) {
            return 0;
        }
        return $ret->count;
    }

    /**
     * @throws DataProviderException
     * @return int
     */
    public function getCount()
    {
        if (null === $this->_count) {
            throw new DataProviderException(sprintf(
                '%s::getData() have to be called before invoking %s',
                __CLASS__,
                __FUNCTION__
            ));
        }
        return $this->_count;
    }
}