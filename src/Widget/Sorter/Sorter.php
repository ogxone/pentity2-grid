<?php
/**
 * Created by PhpStorm.
 * User: ogxone
 * Date: 11.09.15
 * Time: 17:32
 */

namespace Pentity2\Grid\Widget\Sorter;


use Pentity2\Grid\Exception\WidgetException;
use Pentity2\Grid\Widget\AbstractWidget;
use Pentity2\Utils\Helpers\StaticHtmlHelper as Html;

class Sorter extends AbstractWidget
{
    private $_baseUrl;
    private $_query;

    /**
     * @return Array
     */
    protected function _getDefaultConfig()
    {
        return [
            'desc_label' => 'desc',
            'asc_label' => 'asc',
            'desc_class' => 'current desc',
            'asc_class' => 'current asc',
            'param_name' => 'sort',
            'param_field' => null,
            'base_url' => $_SERVER['DOCUMENT_URI'],
            'query' => $_GET,
            'param_sort' => null,
            'content' => null
        ];
    }

    public function setBaseUrl($url)
    {
        $this->_baseUrl = $url;
    }

    public function setQuery(Array $query)
    {
        $this->_query = $query;
    }

    public function getQuery()
    {
        return $this->_query;
    }

    public function getBaseUrl()
    {
        return $this->_baseUrl;
    }

    public function render()
    {
        $orderAsc = $this->getOption('asc_label');
        $orderDesc = $this->getOption('desc_label');

        $paramName = $this->getOption('param_name');
        $paramField = $this->getOption('param_field');
        $paramDirection = $orderAsc;

        $class = '';

        if (
            isset($this->_query[$paramName]) &&
            is_array($this->_query[$paramName])
        ) {
            $param = $this->_query[$paramName];
            $sortField = key($param);
            $sortDirection = current($param);
            if ($sortField == $paramField) {
                if ($orderAsc == $sortDirection) {
                    $paramDirection = $orderDesc;
                    $class = $this->getOption('asc_class');
                } else {
                    $paramDirection = $orderAsc;
                    $class = $this->getOption('desc_class');
                }
            }
        }
        if (isset($this->_query[$paramName])) {
            unset($this->_query[$paramName]);
        }
        $this->_query[$paramName][$paramField] = $paramDirection;
        return Html::a(
            $this->getOption('content'),
            $this->_baseUrl . '?' . urldecode(http_build_query($this->_query)),
            ['class' => $class]
        );
    }

    protected function _validateInput()
    {
        if (null === $this->getOption('param_field')) {
            throw new WidgetException('Parameter param_field should be provided for the sorter');
        }
        if (null === $this->getOption('content')) {
            throw new WidgetException('Parameter content should be provided for the sorter');
        }
    }
}