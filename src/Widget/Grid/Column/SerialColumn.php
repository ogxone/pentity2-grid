<?php
/**
 * Created by PhpStorm.
 * User: ogxone
 * Date: 10.09.15
 * Time: 21:15
 */

namespace Pentity2\Grid\Widget\Grid\Column;


use Pentity2\Domain\Entity\EntityInterface;
use Pentity2\Utils\Helpers\StaticHtmlHelper as Html;

class SerialColumn extends AbstractColumn
{
    private $_counter = 1;
    protected $_title = '#';

    public function renderDataCell(EntityInterface $entity)
    {
        return Html::tag('td', $this->_counter++, $this->getAttributes());
    }
}