<?php
/**
 * Created by PhpStorm.
 * User: ogxone
 * Date: 11.09.15
 * Time: 15:28
 */

namespace Pentity2\Grid\Widget\Grid\Column\Filter\Factory;


use Pentity2\Grid\Widget\Grid\Column\Filter\AbstractFilter;
use Pentity2\Grid\Widget\Grid\Column\Filter\Exception\FilterException;
use Pentity2\Utils\Param\Param;

class FilterFactory
{
    private static $_filters = [
        'text' => 'Pentity2\Grid\Widget\Grid\Column\Filter\TextFilter'
    ];

    /**
     * @param $spec
     * @return AbstractFilter
     * @throws FilterException
     */
    public static function factory($spec)
    {
        $type = Param::strict('type', $spec, 'Filter type have to be provided');
        $options = Param::getArr('options', $spec);

        if (!isset(static::$_filters[$type])) {
            throw new FilterException(sprintf('Undefined filter type %s', $type));
        }
        return new static::$_filters[$type]($options);
    }
}