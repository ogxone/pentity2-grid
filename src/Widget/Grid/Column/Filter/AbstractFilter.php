<?php
/**
 * Created by PhpStorm.
 * User: ogxone
 * Date: 11.09.15
 * Time: 13:19
 */

namespace Pentity2\Grid\Widget\Grid\Column\Filter;


use Pentity2\Grid\Widget\AbstractWidgetComponent;
use Pentity2\Grid\Widget\AttributesAwareTrait;
use Pentity2\Grid\Widget\RenderableInterface;

abstract class AbstractFilter extends AbstractWidgetComponent implements RenderableInterface
{
    use AttributesAwareTrait;

    protected $_value;

    public function setValue($value)
    {
        $this->_value = $value;
    }

    public function getValue()
    {
        return $this->_value;
    }

    /**
     * @return Array
     */
    protected function _getDefaultConfig()
    {
        return [
            'value' => '',
            'attributes' => [

            ]
        ];
    }
}