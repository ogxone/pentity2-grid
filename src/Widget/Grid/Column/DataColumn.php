<?php
/**
 * Created by PhpStorm.
 * User: ogxone
 * Date: 10.09.15
 * Time: 15:56
 */

namespace Pentity2\Grid\Widget\Grid\Column;


use Pentity2\Domain\Entity\EntityInterface;
use Pentity2\Grid\Exception\WidgetException;
use Pentity2\Grid\Widget\Grid\Column\Filter\Factory\FilterFactory;
use Pentity2\Grid\Widget\Sorter\Sorter;
use Pentity2\Utils\ArrayUtils\ArrayUtils;
use Pentity2\Utils\Helpers\StaticHtmlHelper as Html;

class DataColumn extends AbstractColumn implements Filterable
{
    private $_filterConfig = [];
    private $_sorterConfig = [];
    private $_filter;

    public function renderDataCell(EntityInterface $entity)
    {
        if (null !== ($value = $this->getOption('value'))) { //dynamic field
            if (!is_callable($value)) {
                throw new WidgetException(sprintf(
                    'Value options have to be callable. %s given', gettype($value)));
            }
            $content = call_user_func_array($value, [$entity, $this]);
        } else {
            $content = $entity->getField($this->getName());
        }
        if ($this->getOption('encode')) {
            $content = Html::encode($content);
        }
        return Html::tag('td', $content, $this->getAttributes());
    }

    public function setFilterConfig(Array $filterConfig)
    {
        $this->_filterConfig = ArrayUtils::merge($this->_filterConfig, $filterConfig);
    }

    public function getFilterConfig()
    {
        return $this->_filterConfig;
    }

    public function setSorterConfig(Array $sorterConfig)
    {
        $this->_sorterConfig = ArrayUtils::merge($this->_sorterConfig, $sorterConfig);
    }

    public function getSorterConfig()
    {
        return $this->_sorterConfig;
    }

    public function getFilter()
    {
        if (null === $this->_filter) {
            $this->_filter = FilterFactory::factory($this->getFilterConfig());
        }
        return $this->_filter;
    }

    public function renderFilterCell($value = '')
    {
        if (!$this->getOption('filter_enabled')) {
            return '';
        }
        $filter = $this->getFilter();
        $filter->setValue($value);
        $filter->setAttribute('placeholder', $this->getTitle());
        return Html::tag('td', $filter->render(), []);
    }

    protected function _prepareHeaderCell($content)
    {
        if ($this->getOption('sorter_enabled')) {
            return Sorter::init($this->getSorterConfig() + [
                'content' => $content,
                'param_field' => $this->getName()
            ])->render();
        }
        return $content;
    }

    protected function _getDefaultConfig()
    {
        return array_merge(parent::_getDefaultConfig(), [
            'value' => null,
            'filter_enabled' => true,
            'filter_config' => [
                'type' => 'text',
                'options' => []
            ],
            'sorter_enabled' => false,
            'sorter_config' => [

            ]
        ]);
    }
}