<?php
/**
 * Created by PhpStorm.
 * User: ogxone
 * Date: 13.09.15
 * Time: 13:57
 */

namespace Pentity2\Grid\DataProvider;


trait DataProviderAwareTrait
{
    /**
     * @var $_columns DataProviderInterface
     */
    private $_dataProvider;

    public function setDataProvider(DataProviderInterface $dataProvider = null)
    {
        $this->_dataProvider = $dataProvider;
    }

    public function getDataProvider()
    {
        return $this->_dataProvider;
    }
}