<?php
/**
 * Created by PhpStorm.
 * User: ogxone
 * Date: 10.09.15
 * Time: 15:14
 */

namespace Pentity2\Grid\DataProvider;


interface DataProviderInterface
{
    public function getData();
    public function getCount();
    public function getTotalCount();
    public function setPaginationEnabled($paginationEnabled);
    public function getPaginationEnabled();
    public function setPageSize($size);
    public function getPageSize();
    public function getPage();
}