<?php
/**
 * Created by PhpStorm.
 * User: ogxone
 * Date: 14.09.15
 * Time: 17:06
 */

namespace Pentity2\Grid\Widget\Grid\Column;


use Pentity2\Domain\Entity\EntityInterface;
use Pentity2\Grid\Exception\WidgetException;
use Pentity2\Utils\Helpers\StaticHtmlHelper as Html;

class ButtonColumn extends AbstractColumn
{
    private $_cellAttributes;

    protected function _prepareHeaderCell($content)
    {
        return '';
    }

    public function renderDataCell(EntityInterface $entity)
    {
        if (is_callable($hrefCreator = $this->getOption('href'))) {
            $href = call_user_func_array($hrefCreator, [$entity, $this]);
        } else {
            $href = $this->getOption('href') . '/' . $entity->getIdField();
        }
        $content = $this->getOption('content');
        if ($this->getOption('encode')) {
            $content = Html::encode($content);
        }
        return Html::tag(
            'td',
            Html::a($content, $href, $this->getAttributes()),
            $this->getCellAttributes()
        );
    }

    public function setCellAttributes(Array $attributes)
    {
        $this->_cellAttributes = $attributes;
    }

    public function getCellAttributes()
    {
        return $this->_cellAttributes;
    }

    protected function _getDefaultConfig()
    {
        return parent::_getDefaultConfig() + [
            'cell_attributes' => [],
            'base_url' => $_SERVER['DOCUMENT_URI'],
            'content' => '',
            'href' => null,
        ];
    }

    protected function _validateInput()
    {
        if (!(
            is_string($this->getOption('href')) ||
            is_callable($this->getOption('href'))
        )) {
            throw new WidgetException(
                'Invalid button specification. Href have to be either string or callable');
        }
    }
}