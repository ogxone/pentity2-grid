<?php
/**
 * Created by PhpStorm.
 * User: ogxone
 * Date: 11.09.15
 * Time: 15:31
 */

namespace Pentity2\Grid\Widget\Grid\Column\Filter\Exception;


use Pentity2\Grid\Exception\WidgetException;

class FilterException extends WidgetException
{

}