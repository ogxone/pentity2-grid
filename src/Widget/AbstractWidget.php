<?php
/**
 * Created by PhpStorm.
 * User: ogxone
 * Date: 10.09.15
 * Time: 14:20
 */

namespace Pentity2\Grid\Widget;


abstract class AbstractWidget extends AbstractWidgetComponent implements RenderableInterface
{
    /**
     * @var $_instance AbstractWidget
    */
    protected static $_instances = [];

    /**
     * @param array $params
     * @return AbstractWidget
     */
    public static function init(Array $params)
    {
        $className = get_called_class();
        if (!isset(static::$_instances[$className])) {
            static::$_instances[$className] = new static($params);
        } else {
            static::$_instances[$className]->configure($params);
        }

        return static::$_instances[$className];
    }
}