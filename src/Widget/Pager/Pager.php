<?php
/**
 * Created by PhpStorm.
 * User: ogxone
 * Date: 13.09.15
 * Time: 13:40
 */

namespace Pentity2\Grid\Widget\Pager;


use Pentity2\Grid\DataProvider\DataProviderAwareInterface;
use Pentity2\Grid\DataProvider\DataProviderAwareTrait;
use Pentity2\Grid\DataProvider\DataProviderInterface;
use Pentity2\Grid\Exception\WidgetException;
use Pentity2\Grid\Widget\AbstractWidget;


class Pager extends AbstractWidget implements DataProviderAwareInterface
{
    use DataProviderAwareTrait;

    private $_template;
    private $_page;
    private $_pageSize;
    private $_totalCount;
    private $_count;
    private $_totalTemplate;
    private $_query;
    private $_baseUrl;
    private $_perpageOptions;

    public function setTemplate($template)
    {
        if (!is_readable($template)) {
            throw new WidgetException(sprintf('Unreadable template %s', $template));
        }
        $this->_template = $template;
    }

    public function setBaseUrl($url)
    {
        $this->_baseUrl = $url;
    }

    public function setQuery(Array $query)
    {
        $pageParam = $this->getOption('page_param');
        if (!isset($query[$pageParam])) {
            unset($query[$pageParam]);
        }
        $this->_query = $query;
    }

    public function getQuery()
    {
        return $this->_query;
    }

    public function getBaseUrl()
    {
        return $this->_baseUrl;
    }

    public function getPage()
    {
        if (null === $this->_page) {
            $this->_page = $this->getDataProvider()->getPage();
        }
        return $this->_page;
    }

    public function setPageSize($size)
    {
        $this->getDataProvider()->setPageSize($size);
    }

    public function getPageSize()
    {
        if (null === $this->_pageSize) {
            $this->_pageSize = $this->getDataProvider()->getPageSize();
        }
        return $this->_pageSize;
    }

    public function getTotalCount()
    {
        if (null === $this->_totalCount) {
            $this->_totalCount = $this->getDataProvider()->getTotalCount();
        }
        return $this->_totalCount;
    }

    public function getCount()
    {
        if (null === $this->_count) {
            $this->_count = $this->getDataProvider()->getCount();
        }
        return $this->_count;
    }

    public function getTemplate()
    {
        return $this->_template;
    }

    public function setPerpageOptions(Array $options)
    {
        foreach ($options as $option) {
            if (!is_int($option)) {
                throw new WidgetException(sprintf('Perpage options have to be an array of integers. %s given', gettype($option)));
            }
        }
        $this->_perpageOptions = array_unique($options);
    }

    public function getPerpageOptions()
    {
        return $this->_perpageOptions;
    }

    public function isFirstDisabled()
    {
        return $this->getDataProvider()->getPage() == 1 ;
    }

    public function isPrevDisabled()
    {
        return $this->getDataProvider()->getPage() == 1 ;
    }

    public function isNextDisabled()
    {
        $lastPage = ceil($this->getTotalCount() / $this->getPageSize());
        return $this->getPage() >= $lastPage ;
    }

    public function isLastDisabled()
    {
        $lastPage = ceil($this->getTotalCount() / $this->getPageSize());
        return $this->getPage() >= $lastPage ;
    }

    public function setTotalTemplate($template)
    {
        if (substr_count($template, '%s') !== 3) {
            throw new WidgetException(sprintf('Total template have to have exactly 3 sprintf placeholder'));
        }
        $this->_totalTemplate = $template;
    }

    public function getTotalTemplate()
    {
        return $this->_totalTemplate;
    }

    public function render()
    {
        if (
            $this->getTotalCount() == 0 ||                      //nothing found
            !$this->getDataProvider()->getPaginationEnabled()    //pagination disabled
        ) {
            return '';
        }
        ob_start();
        include $this->_template;
        return ob_get_clean();
    }

    public function renderTotal()
    {
        $start = ($this->getPageSize() * ($this->getPage() - 1)) + 1;
        if ($this->getTotalCount() < $this->getPageSize()) {
            $stop = $this->getTotalCount();
        } else {
            $stop = $start + $this->getPageSize() - 1;
        }
        return sprintf($this->getTotalTemplate(), $start, $stop, $this->getTotalCount());
    }

    public function createPagesList()
    {
        $pagesList = [
            [
                'title' => $currentPage = $this->getPage(),
                'href'=> '#',
                'class' => 'current'
            ]
        ];
        $page = $this->getPage();
        $lastPage = ceil($this->getTotalCount() / $this->getPageSize());
        $showPages = $this->getOption('show_pages');
        if ($showPages > $lastPage) {
            $showPages = $lastPage;
        }
        if (!($showPages & 1)) { //make odd
            $showPages++;
        }
        for ($i = 1; $i < $showPages; $i++) {
            if ($page - $i > 0) {
                array_unshift($pagesList, [
                    'title' => $page - $i,
                    'href' => $this->createPageUrl($page - $i),
                    'class' => '',
                ]);
            }
            if (count($pagesList) >= $showPages) {
                break;
            }
            if ($page + $i <= $lastPage) {
                array_push($pagesList, [
                    'title' => $page + $i,
                    'href' => $this->createPageUrl($page + $i),
                    'class' => '',
                ]);
            }
            if (count($pagesList) >= $showPages) {
                break;
            }
        }
        return $pagesList;
    }

    public function createPerPageUrl($perPage)
    {
        $query = $this->getQuery();
        $pageParam = $this->getOption('page_param');
        $perPageParam = $this->getOption('perpage_param');
        if (isset($query[$pageParam])) {
            unset($query[$pageParam]);
        }
        if (isset($query[$perPageParam])) {
            unset($query[$perPageParam]);
        }
        $query[$perPageParam] = $perPage;
        return $this->getBaseUrl() . '?' . http_build_query($query);
    }

    public function createPageUrl($page)
    {
        $query = $this->getQuery();
        $pageParam = $this->getOption('page_param');
        $query[$pageParam] = $page;
        return $this->getBaseUrl() . '?' . http_build_query($query);
    }

    public function createNextUrl()
    {
        return $this->createPageUrl($this->getPage() + 1);
    }

    public function createLastUrl()
    {
        return $this->createPageUrl(ceil($this->getTotalCount() / $this->getPageSize()));
    }

    public function createFirstUrl()
    {
        return $this->createPageUrl(1);
    }

    public function createPrevUrl()
    {
        return $this->createPageUrl($this->getPage() <= 1 ? 1 : $this->getPage() - 1);
    }

    /**
     * @throws WidgetException
     */
    protected function _validateInput()
    {
        if (!$this->getDataProvider() instanceof DataProviderInterface) {
            throw new WidgetException(sprintf(
                'data provider have to implement Pentity2\Grid\DataProvider\DataProviderInterface'));
        }
    }

    /**
     * @return Array
     */
    protected function _getDefaultConfig()
    {
        return [
            'data_provider' => null,
            'template' => GRID_DEFAULT_VIEW_PATH . '/templates/pager.phtml',
            'page_param' => 'page',
            'show_pages' => 5,
            'box_class' => 'dPagination',
            'current_class' => 'current',
            'disabled_class' => 'disabled',
            'show_start_stop' => true,
            'show_prev_next' => true,
            'start_class' => 'first',
            'next_class' => 'next',
            'prev_class' => 'prev',
            'last_class' => 'last',
            'show_total' => true,
            'total_class' => 'total',
            'total_template' => '%s - %s of %s items found.',
            'base_url' => $_SERVER['DOCUMENT_URI'],
            'query' => $_GET,
            'perpage_param' => 'page_size',
            'perpage_options' => [10, 20, 30],
            'perpage_label' => 'per page'
        ];
    }
}