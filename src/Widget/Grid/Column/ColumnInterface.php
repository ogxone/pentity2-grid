<?php
/**
 * Created by PhpStorm.
 * User: ogxone
 * Date: 10.09.15
 * Time: 15:54
 */

namespace Pentity2\Grid\Widget\Grid\Column;


use Pentity2\Domain\Entity\EntityInterface;

interface ColumnInterface
{
    public function getName();
    public function renderDataCell(EntityInterface $entity);
    public function renderHeaderCell();
//    public function renderFooterCell();
}