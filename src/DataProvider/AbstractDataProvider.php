<?php
/**
 * Created by PhpStorm.
 * User: ogxone
 * Date: 10.09.15
 * Time: 17:33
 */

namespace Pentity2\Grid\DataProvider;


use Pentity2\Grid\Widget\AbstractWidgetComponent;

abstract class AbstractDataProvider extends AbstractWidgetComponent implements DataProviderInterface
{
    protected $_pageSize;
    protected $_page;
    protected $_paginationEnabled;

    protected function _getDefaultConfig()
    {
        return [
            'page_size' => 10,
            'page' => 1,
            'pagination_enabled' => false
        ];
    }

    public function setPaginationEnabled($paginationEnabled)
    {
        $this->_paginationEnabled = (bool)$paginationEnabled;
    }

    public function getPaginationEnabled()
    {
        return $this->_paginationEnabled;
    }

    public function setPageSize($pageSize)
    {
        $pageSize = (int)$pageSize;
        if ($pageSize <= 0) {
            $pageSize = 10;
        }
        $this->_pageSize = $pageSize;
    }

    public function setPage($page)
    {
        $page = (int)$page;
        if ($page <= 0) {
            $page = 1;
        }
        $this->_page = $page;
    }

    public function getPageSize()
    {
        return $this->_pageSize;
    }

    public function getPage()
    {
        return $this->_page;
    }
}