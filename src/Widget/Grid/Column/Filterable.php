<?php
/**
 * Created by PhpStorm.
 * User: ogxone
 * Date: 11.09.15
 * Time: 15:22
 */

namespace Pentity2\Grid\Widget\Grid\Column;


interface Filterable 
{
    public function renderFilterCell();
}