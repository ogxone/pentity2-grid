<?php
/**
 * Created by PhpStorm.
 * User: ogxone
 * Date: 18.09.15
 * Time: 11:31
 */

namespace Pentity2\Grid\Widget;


use Pentity2\Utils\ArrayUtils\ArrayUtils;

trait AttributesAwareTrait
{
    protected $_attributes = [];

    public function setAttributes(Array $attributes)
    {
        $this->_attributes = ArrayUtils::merge($this->_attributes, $attributes);
    }

    public function setAttribute($name, $value)
    {
        $this->_attributes[$name] = $value;
    }

    public function getAttributes()
    {
        return $this->_attributes;
    }
}