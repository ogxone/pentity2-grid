<?php
/**
 * Created by PhpStorm.
 * User: ogxone
 * Date: 18.09.15
 * Time: 11:08
 */

namespace Pentity2\Grid\Widget\Grid\Action\Button;


use Pentity2\Utils\ArrayUtils\ArrayUtils;
use Pentity2\Utils\Helpers\StaticHtmlHelper as Html;

class PlainButton extends AbstractButton
{
    public function render()
    {
        $label = $this->getOption('label');
        if ($this->getOption('encode_label')) {
            $label = Html::encode($label);
        }
        return Html::tag(
            $this->getOption('tag'),
            $label,
            $this->getAttributes()
        );
    }

    /**
     * @return Array
     */
    protected function _getDefaultConfig()
    {
        return ArrayUtils::merge(parent::_getDefaultConfig(), [
            'tag' => 'span',
            'label' => 'Button',
            'attributes' => [
                'class' => 'button'
            ]
        ]);
    }
}